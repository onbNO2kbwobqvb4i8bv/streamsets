package com.streamsets.graph;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;

public class GraphTest {

    private static final String EOL = System.getProperty("line.separator");
    private PrintStream stdout;
    private InputStream stdin;
    private ByteArrayOutputStream bytes;

    @Before
    public void setUp() {
        bytes = new ByteArrayOutputStream();
        stdout = System.out;
        System.setOut(new PrintStream(bytes));
        stdin = System.in;
    }

    @After
    public void tearDown() {
        System.setOut(stdout);
        System.setIn(stdin);
    }

    @Test
    public void shouldPrintUsage() throws IOException {
        Graph.main(new String[] {"A"});
        assertEquals(Graph.HELP, bytes.toString());
    }

    @Test
    public void shouldPrintConnectedComponents() throws IOException {
        System.setIn(new ByteArrayInputStream(("1 2 3 2 4 5" + EOL + EOL).getBytes()));
        Graph.main(new String[0]);
        assertEquals("1 2 3 4 5" + EOL, bytes.toString());
    }
}
