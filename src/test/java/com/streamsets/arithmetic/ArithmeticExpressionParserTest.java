package com.streamsets.arithmetic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.script.ScriptException;
import java.io.*;

import static org.junit.Assert.assertEquals;

public class ArithmeticExpressionParserTest {

    private static final String EOL = System.getProperty("line.separator");
    private PrintStream stdout;
    private InputStream stdin;
    private ByteArrayOutputStream bytes;

    @Before
    public void setUp() {
        bytes = new ByteArrayOutputStream();
        stdout = System.out;
        System.setOut(new PrintStream(bytes));
        stdin = System.in;
    }

    @After
    public void tearDown() {
        System.setOut(stdout);
        System.setIn(stdin);
    }

    @Test
    public void shouldPrintUsage() throws IOException, ScriptException {
        ArithmeticExpressionParser.main(new String[] {"A"});
        assertEquals(ArithmeticExpressionParser.HELP, bytes.toString());
    }

    @Test
    public void shouldPrintResult() throws IOException, ScriptException {
        System.setIn(new ByteArrayInputStream(("(1+2)*(3+(4*5))" + EOL + EOL).getBytes()));
        ArithmeticExpressionParser.main(new String[0]);
        assertEquals("69" + EOL, bytes.toString());
    }
}
