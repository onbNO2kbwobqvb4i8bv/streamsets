package com.streamsets.robot;

import com.streamsets.robot.device.Robot;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;

public class SimulationTest {

    private static final String EOL = System.getProperty("line.separator");
    private PrintStream stdout;
    private InputStream stdin;
    private ByteArrayOutputStream bytes;

    @Before
    public void setUp() {
        bytes = new ByteArrayOutputStream();
        stdout = System.out;
        System.setOut(new PrintStream(bytes));
        stdin = System.in;
    }

    @After
    public void tearDown() {
        System.setOut(stdout);
        System.setIn(stdin);
    }

    @Test
    public void shouldPrintCurrentPosition() throws IOException {
        System.setIn(new ByteArrayInputStream(("LMLMLMLMMRMRMRRM" + EOL + EOL).getBytes()));
        Simulation.main(new String[]{"100", "100", "1", "2", "N"});
        assertEquals("2 3 N", bytes.toString().split(EOL)[1]);
    }

    @Test
    public void shouldLeavePlateau() throws IOException {
        System.setIn(new ByteArrayInputStream(("LMM" + EOL + EOL).getBytes()));
        Simulation.main(new String[]{"100", "100", "1", "2", "N"});
        assertEquals(Robot.RECEIVER_STARTED + Simulation.GAME_OVER, bytes.toString());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldThrowUnsupportedOperationException() throws IOException {
        System.setIn(new ByteArrayInputStream(("B" + EOL + EOL).getBytes()));
        Simulation.main(new String[]{"100", "100", "1", "2", "N"});
    }

    @Test
    public void shouldPrintUsageForMisconfiguration() throws IOException {
        Simulation.main(new String[]{"B", "100", "1", "2", "N"});
        assertEquals(Simulation.HELP, bytes.toString());
    }

    @Test
    public void shouldPrintUsageForWrongNumberOfArgs() throws IOException {
        Simulation.main(new String[]{"B", "100", "1", "2"});
        assertEquals(Simulation.HELP, bytes.toString());
    }
}
