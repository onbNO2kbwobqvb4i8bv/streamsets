package com.streamsets.arithmetic;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ArithmeticExpressionParser {

    static final String HELP = "The application takes an arithmetic expression from standard input.\n" +
            "Every binary operation must be wrapped in a set of parenthesis.";

    private static void printUsage() {
        System.out.print(HELP);
    }

    public static void main(String[] args) throws IOException, ScriptException {
        if (args.length > 0) {
            printUsage();
            return;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("javascript");
        System.out.println(engine.eval(br.readLine()));
    }
}
