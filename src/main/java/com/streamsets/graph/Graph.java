package com.streamsets.graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Graph {

    static final String HELP = "The application prints all connected components of an undirected graph line by line delimited by one space.\n" +
            "Usage:\n" +
            "\tThe application takes IDs of a graph's components from standard input, multiple per line delimited by one space.\n" +
            "\tAdjacent IDs make a connection between them.\n" +
            "\t\te.g. 1 2 3 2 4 5 means 2 is connected to 1, 3 and 4; 4 is connected to 5\n\n" +
            "\tA graph's configuration is considered to be complete by hitting Enter.\n" +
            "\tIDs are considered unique as per graph, so the same ID might be reused in a separate graph (line) and will be handled separately." +
            "\tApplication configuration is considered to be complete by hitting Enter on an empty line.";

    public static void main(String[] args) throws IOException {
        if (args.length > 0) {
            printUsage();
            return;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        List<String> results = new ArrayList<>();
        while (s.length() > 0) {
            results.add(Arrays.stream(s.split(" ")).distinct().collect(Collectors.joining(" ")));
            s = br.readLine();
        }
        results.forEach(System.out::println);
    }

    private static void printUsage() {
        System.out.print(HELP);
    }
}
