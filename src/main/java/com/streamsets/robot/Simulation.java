package com.streamsets.robot;

import com.streamsets.robot.device.Robot;
import com.streamsets.robot.map.CardinalCompassPoint;
import com.streamsets.robot.map.Plateau;
import com.streamsets.robot.map.RobotLeftPlateau;

import java.io.IOException;

public class Simulation {

    static String HELP = "The application takes control characters from standard input for a robot landed on a plateau at given coordinates.\n" +
            "The plateau is rectangular and is divided up into a grid.\n" +
            "The plateau's dimensions and landing position are taken as program arguments: robot 100 100 1 2 N'\n" +
            "Arguments in order: plateau X dimension, plateau Y dimension, landing X position, landing Y position, heading [NSEW]\n" +
            "To exit hit Enter on empty line.";

    static String GAME_OVER = "The Goa'uld anti-gravity device is offline. Robot lost.";

    public static void main(String[] args) throws IOException {
        if (args.length != 5) {
            printUsage();
            return;
        }
        try {
            Plateau plateau = new Plateau(Long.parseLong(args[0]), Long.parseLong(args[1]));
            Robot.INSTANCE.landOn(plateau, Long.parseLong(args[2]), Long.parseLong(args[3]), CardinalCompassPoint.valueOf(args[4]));
            Robot.INSTANCE.startSignalReceiver();
        } catch (NumberFormatException ex) {
            printUsage();
        } catch (RobotLeftPlateau robotLeftPlateau) {
            printGameOverMessage();
        }
    }

    private static void printUsage() {
        System.out.print(HELP);
    }

    private static void printGameOverMessage() {
        System.out.print(GAME_OVER);
    }
}
