package com.streamsets.robot.device;

import com.streamsets.robot.map.CardinalCompassPoint;
import com.streamsets.robot.map.Plateau;
import com.streamsets.robot.map.RobotLeftPlateau;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Robot {

    public static final String RECEIVER_STARTED = "Signal receiver started...\n";

    private static final String TURN_LEFT = "L";
    private static final String TURN_RIGHT = "R";
    private static final String MOVE_COMMAND = "M";

    private static final String VALID_COMMANDS = TURN_LEFT + TURN_RIGHT + MOVE_COMMAND;

    public static Robot INSTANCE = new Robot();

    private CardinalCompassPoint heading;

    private Plateau plateau;

    private Robot() {
    }

    public void landOn(Plateau plateau, long x, long y, CardinalCompassPoint heading) {
        this.plateau = plateau;
        plateau.placeRobot(x, y);
        this.heading = heading;
    }

    public void startSignalReceiver() throws IOException, RobotLeftPlateau {
        System.out.print(RECEIVER_STARTED);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        while (s.length() > 0) {
            performCommands(parseCommands(s));
            reportPosition();
            s = br.readLine();
        }
    }

    private void reportPosition() {
        System.out.println(plateau.getRobotPositionX() + " " + plateau.getRobotPositionY() + " " + heading.name());
    }

    private void performCommands(String[] parseCommands) throws RobotLeftPlateau {
        for(String command: parseCommands) {
            switch (command) {
                case MOVE_COMMAND: plateau.moveRobot(heading); break;
                case TURN_LEFT: heading = heading.left(); break;
                case TURN_RIGHT: heading = heading.right(); break;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    private String[] parseCommands(String s) {
        validateCommands(s);
        return s.split("");
    }

    private void validateCommands(String s) {
        if (!s.matches("[" + VALID_COMMANDS + "]+")) {
            throw new UnsupportedOperationException();
        }
    }
}
