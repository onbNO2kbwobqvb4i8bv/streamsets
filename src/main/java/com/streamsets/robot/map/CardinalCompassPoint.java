package com.streamsets.robot.map;

public enum CardinalCompassPoint {
    N(0, 1), S(0, -1), W(-1, 0), E(1, 0);

    private final long modifierX;
    private final long modifierY;

    CardinalCompassPoint(long modifierX, long modifierY) {
        this.modifierX = modifierX;
        this.modifierY = modifierY;
    }

    public long getModifierX() {
        return modifierX;
    }

    public long getModifierY() {
        return modifierY;
    }

    public CardinalCompassPoint left() {
        switch (this) {
            case N: return W;
            case W: return S;
            case S: return E;
            case E: return N;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public CardinalCompassPoint right() {
        switch (this) {
            case N: return E;
            case E: return S;
            case S: return W;
            case W: return N;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
