package com.streamsets.robot.map;

public class Plateau {
    private final long dimensionX;
    private final long dimensionY;

    private long robotPositionX;
    private long robotPositionY;

    public Plateau(long dimensionX, long dimensionY) {
        this.dimensionX = dimensionX;
        this.dimensionY = dimensionY;
    }

    public void placeRobot(long x, long y) {
        robotPositionX = x;
        robotPositionY = y;
    }

    public void moveRobot(CardinalCompassPoint heading) throws RobotLeftPlateau {
        robotPositionX += heading.getModifierX();
        robotPositionY += heading.getModifierY();
        if (isOffPlateau(robotPositionX, robotPositionY)) {
            throw new RobotLeftPlateau();
        }
    }

    private boolean isOffPlateau(long robotPositionX, long robotPositionY) {
        return  robotPositionX < 0 || robotPositionY < 0 || robotPositionX > dimensionX || robotPositionY > dimensionY;
    }

    public long getRobotPositionX() {
        return robotPositionX;
    }

    public long getRobotPositionY() {
        return robotPositionY;
    }
}
